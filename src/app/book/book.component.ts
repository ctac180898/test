import { Component, OnInit } from '@angular/core';
import {DbService} from '../services/db.service';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  // Observable arrays
  $formats;
  $countries;
  $cities;
  $companies;

  // Arrays
  cities;
  companies;

  cityChecked = false;
  bookForm: FormGroup;
  submitted = false;
  id;

  constructor( private db: DbService, private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadFormData();
    this.makeForm();
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
      this.db.makeRequest('get', 'books', this.id).subscribe(book => {
        this.fillForm(book);
      });
    });
  }

  makeForm() {
    this.bookForm = this.formBuilder.group({
      author: ['', [Validators.required, Validators.maxLength(30), Validators.minLength(5)]],
      title: ['', [Validators.required, Validators.maxLength(35)]],
      isbn: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      pages: ['', Validators.required],
      formatId: ['', Validators.required],
      description: ['', [Validators.required, Validators.maxLength(500)]],
      price: ['', Validators.required],
      countryId: ['', Validators.required],
      cityId: ['', Validators.required],
      companyId: ['', Validators.required],
    });
  }

  loadFormData() {
    this.$formats = this.db.makeRequest('get', 'formats');
    this.$countries = this.db.makeRequest('get', 'countries');
    this.$cities = this.db.makeRequest('get', 'cities');
    this.$companies = this.db.makeRequest('get', 'companies')
    this.$cities.subscribe(cities => {
      this.cities = cities;
    });
    this.$companies.subscribe(companies => {
      this.companies = companies;
    });
  }

  fillForm(book) {
    this.bookForm.controls['author'].setValue(book.author);
    this.bookForm.controls['title'].setValue(book.title);
    this.bookForm.controls['isbn'].setValue(book.isbn);
    this.bookForm.controls['pages'].setValue(book.pages);
    this.bookForm.controls['formatId'].setValue(book.formatId);
    this.bookForm.controls['description'].setValue(book.description);
    this.bookForm.controls['price'].setValue(book.price);
    this.bookForm.controls['countryId'].setValue(book.countryId);
    this.bookForm.controls['cityId'].setValue(book.cityId);
    this.bookForm.controls['companyId'].setValue(book.companyId);
  }

  checkCity(): boolean {

    for (const city of this.cities){

      if (city.id.toString() === this.bookForm.value.cityId.toString() &&
        city.countryId.toString() === this.bookForm.value.countryId.toString()) {

        for (const company of this.companies){
          if (company.id.toString() === this.bookForm.value.companyId.toString() &&
          company.cityId.toString() === this.bookForm.value.cityId.toString()) {
            this.cityChecked = true;
            return true;
          }
        }

      }

    }
    return false;
  }

  get form() { return this.bookForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.bookForm.invalid || !this.checkCity()) {
      return;
    }
    console.log(this.bookForm.value);
    this.db.makeRequest('add', 'books', this.bookForm.value).subscribe(response => {
      console.log(response);
    });
  }

}
