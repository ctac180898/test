import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Book} from '../book/book.model';
import {DbService} from '../services/db.service';

@Component({
  selector: 'app-showcase',
  templateUrl: './showcase.component.html'
})
export class ShowcaseComponent implements OnInit {

  $books: Observable<Book[]>;
  constructor( private db: DbService) { }

  ngOnInit() {
    this.$books = this.db.makeRequest('get', 'books');
  }

}
