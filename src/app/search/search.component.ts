import { Component, OnInit } from '@angular/core';
import {DbService} from '../services/db.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { Location } from '@angular/common';
import {Observable} from 'rxjs/Observable';
import {Book} from '../book/book.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {


  $books: Observable<Book[]>;
  $formats;
  submitted = false;
  searchBookForm: FormGroup;
  searchUrl = '?';

  constructor( private db: DbService,
               private formBuilder: FormBuilder,
               private location: Location,
               private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.$formats = this.db.makeRequest('get', 'formats');
    this.makeForm();
    this.setFormListener();
    this.fillForm();
  }

  makeForm() {
    this.searchBookForm = this.formBuilder.group({
      author: ['', [Validators.maxLength(30)]],
      title: ['', [Validators.maxLength(35)]],
      isbn: ['', [Validators.maxLength(10)]],
      formatId: [''],
      pages_lte: [''],
      pages_gte: [''],
      price_lte: [''],
      price_gte: [''],
    });
  }

  fillForm() {
    this.activatedRoute.queryParams.subscribe(params => {
      for (const key in params) {
        if ( params[key]) {
          this.searchBookForm.controls[key].setValue(params[key]);
        }
      }
    });
  }

  setFormListener() {
    this.searchBookForm.valueChanges.subscribe(value => {
      this.searchUrl = '?';
      for ( const key in value) {
        if (value[key]) {
          this.searchUrl += (`${key}=` + value[key] + '&');
        }
      }
      this.searchUrl = this.searchUrl.slice(0, -1);
      this.location.replaceState(`search/` + this.searchUrl );
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.searchBookForm.invalid) {
      return;
    }
    this.$books = this.db.makeRequest('get', 'books', this.searchUrl);
  }

}
