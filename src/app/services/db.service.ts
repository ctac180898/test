import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';
import {Book} from '../book/book.model';

@Injectable()
export class DbService {

  url = 'http://localhost:3004';

  httpOptions = {
    headers: new HttpHeaders({
      'x-auth-token':  'bad18eba1ff45jk7858b8ae88a77fa30',
    })
  };

  constructor(private http: HttpClient) {
  }

  private request(type: 'add' | 'get', endpoint: 'formats' | 'countries' | 'cities' | 'books' | 'companies', item?): Observable<any> {
    let base;

    if (type === 'add') {
      base = this.http.post(this.url + `/books/`, item, this.httpOptions);
    } else if (item && type === 'get') {
      base = this.http.get(this.url + '/' + endpoint + '/' + item, this.httpOptions);
    } else if (type === 'get') {
      base = this.http.get(this.url + '/' + endpoint, this.httpOptions);
    }

    const request = base.pipe(
      map((data: any) => {
        return data;
      })
    );

    return request;
  }

  public makeRequest(type: 'add' | 'get', endpoint: 'formats' | 'countries' | 'cities' | 'books' | 'companies', item?): Observable<any> {
    return this.request(type, endpoint, item);
  }
}
